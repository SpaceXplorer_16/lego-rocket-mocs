<div class="navbar">
[Top](index.html#webtop) | [TOC](index.html#table-of-contents) | [Sources](index.html#main-sources-of-mocs) | [**Rockets**](index.html#table-of-rockets-and-spacecrafts) | [**Payloads**](index.html#table-of-satellites-and-probes-payload) | [**Others**](index.html#table-of-launchpads-stands-etc.) | [**Fallon's ISS**](index.html#iss-by-dan-fallon) | [LEGO sets](index.html#table-of-official-lego-sets) | [Comparisons](index.html#comparisons) | [Candidates](candidates.html) | [Honorary](index.html#honorary-mentions) | [Changes](https://gitlab.com/KaeroDot/lego-rocket-mocs/-/commits/master)
</div>

# List of found space projects that were considered as candidates
If there is X before the link, it means I already tried to find instructions and failed. If there is no X, I have yet to check it. If the instructions was found, the model was moved to the lists above.

## Projects found on Lego Ideas
**Eiffleman**
X [NASA Space Launch System](https://ideas.lego.com/projects/056aadba-d966-4d7b-b689-ea88bfc5e12f/updates#content_nav_tabs)
**SpaceXandLEGOfan**
X [Falcon Heavy/9 convertable](https://ideas.lego.com/projects/61ff1576-7432-4d8a-9336-e9a3088e4c48)
**Matthew Nolan**
X [SpaceX – The Ultimate Collection](https://ideas.lego.com/projects/7457e3ff-0397-42e1-b37d-fc54c3c651ad) - Not approved on Lego Ideas.
X [Blue Origin New Glenn Rocket, Launch Tower & Blue Moon Lander - 1/110 Scale Set](https://ideas.lego.com/projects/13e68955-084b-41fc-a4c8-8f01b552f84c)
X [NASA's SLS & Artemis](https://ideas.lego.com/projects/e72800b7-14d6-40b4-b9f1-d9a60efc9e45)
**Renebricks2858**
X [ULA Delta IV Heavy](https://ideas.lego.com/projects/453a4e94-a48b-41f2-af78-de5cab3fe135)
**KingsKnight**
X [NASA Space Shuttle (Saturn V Scale)](https://ideas.lego.com/projects/50a447cc-0acb-4fff-b3c9-41739fed157c/updates#content_nav_tabs) - Not approved on Lego Ideas. Maybe instructions on eurobricks? [yes!](https://www.eurobricks.com/forum/index.php?/forums/topic/152554-moc-lego-ideas-space-shuttle-saturn-v-scale/&page=6)
**khehmeyer**
X [SpaceX Falcon Heavy](https://ideas.lego.com/projects/1d1b481c-e8b4-4dda-99f3-ec6243139099/updates?project_updates_page=2) - Not approved on Lego Ideas.
**Graupensuppe**
X [Vostok 1 - The First Manned Spaceflight](https://ideas.lego.com/projects/0630bb23-59e5-4eb4-8ba0-a1c88c5e8602), [Lego Ideas](https://www.eurobricks.com/forum/index.php?/forums/topic/164340-moc-vostok-1-the-first-manned-spaceflight/&tab=comments#comment-3005900), [Eurobricks](https://www.eurobricks.com/forum/index.php?/forums/topic/164340-moc-vostok-1-the-first-manned-spaceflight/)
**Stevenhoward27**
X [Soyuz Rocket](https://ideas.lego.com/projects/7a3be173-1a90-4941-b171-5531ee59d6f3/updates#content_nav_tabs)
X [Mini-fig Scale Project Gemini](https://ideas.lego.com/projects/473e666e-0c05-4cd5-9a3c-fcde78e09e7e)
**Lego_Aviator**
X [SpaceX Falcon 9 (Saturn V Scale)](https://ideas.lego.com/projects/1abc6458-52e8-4e7d-a04c-04ba917b6e5b), [MOCpages](http://www.moc-pages.com/moc.php/445154)
**Howlrunner**
X [Soyuz spacecraft](https://ideas.lego.com/projects/10585299-0f05-4f75-9b10-f8a93b6b8e18)
**microdragons**
X [Apollo 11 Saturn V](https://ideas.lego.com/projects/5e0f24c7-4bf1-4fd1-91e3-52a2bfd49834)
**kevinszeto**
X [Virgin Galactic](https://ideas.lego.com/projects/2e0fd742-4208-4dc7-af64-b18439c9bc98)
**Tahoe2013DD**
X [NASA Juno Spacecraft.](https://ideas.lego.com/projects/d80e8782-ee2f-45e9-b4fa-264df106ba5d)
**tttoshi**
X [Vostok 1](https://ideas.lego.com/projects/4901c181-c61c-40f8-ab7c-018fb16967f4)
**nddragoon**
X [Mars Sojourner](https://ideas.lego.com/projects/9ec66020-9813-4826-baca-f2193dda24f5/updates#content_nav_tabs)
**ruimrbranco**
X [One Small Step](https://ideas.lego.com/projects/2a761df0-3e6b-4604-a4bf-0a677002857c)
**Alex Pendable**
X [Ariane 6](https://ideas.lego.com/projects/fbb6b2e9-21f2-409a-b899-91885db3a5ab)
**LegoEddy8991**
X [Philae, Comet Lander](https://ideas.lego.com/projects/61f05d01-e0be-4c41-8f66-20a247d4eb2b)
**newman**
X [SpaceX Falcon Heavy](https://ideas.lego.com/projects/8aeeab51-b21d-4961-821b-3724b4382526)
X [Falcon 9 Booster](https://ideas.lego.com/projects/982775a3-466b-460d-90af-33fd140b2838)
**moremilk95**
X [N1-L3 Soviet Moon Rocket](https://ideas.lego.com/projects/03b69fed-1de2-4ccb-9008-9896a2a57644)
X [SLS Orion](https://ideas.lego.com/projects/0c3c85e1-e2c5-4b03-a190-d81bd3d1d859)
**johnmknight**
X [Boeing CST-100 Starliner](https://ideas.lego.com/projects/adf54caf-e424-4502-9254-88cc1b1adbc2)
X [Cygnus Spacecraft](https://ideas.lego.com/projects/578e933f-e17e-4518-bffd-4a74d0c2eb38)
X [Orion Mnifig Scale Capsule and Service Module](https://ideas.lego.com/projects/eaa2410b-fbbb-4f5b-ac5a-12316f34ac6e)
X [Philae Comet Landing](https://ideas.lego.com/projects/3018afe5-2dd5-4c53-a477-252c63061b66)
X [Hubble Repair Mission](https://ideas.lego.com/projects/427457c7-b96b-4835-a710-ddbf3e5a2900)
X [SpaceX Dragon Spacecraft](https://ideas.lego.com/projects/3b6e448e-4651-44fd-84db-01bd3751ceb0)
X [Near Minifig Scale Mars Science Laboratory](https://ideas.lego.com/projects/436f81c6-f6fd-4dc9-a276-90a5eb5191fc)
X [Scale Space Shuttle Atlantis](https://ideas.lego.com/projects/9f0fb6d8-61b5-4d67-a4ac-16a219b6ac3a)
**Captain_SUSHI**
X [Project Gemini Capsule](https://ideas.lego.com/projects/d76b1ee5-4d7f-4aa6-8f35-15e5bab55a87)
X [Voyager Probe](https://ideas.lego.com/projects/ff5f67c1-1966-4773-b9ff-9278f956834a)
X [Philea Lander](https://ideas.lego.com/projects/33f5e93f-b903-4ed9-b27e-6d2013bf2f14)
X [Rosetta Spacecraft](https://ideas.lego.com/projects/bb19b36e-060c-4789-952e-07d21d23b7fd)
X [Desktop Space Shuttle](https://ideas.lego.com/projects/991a4f52-3f11-43c9-9a46-7be5301d1f72)
**MiniBrick Productions**
X [SpaceX Falcon Heavy](https://ideas.lego.com/projects/8d4ed79c-c06f-40c5-b3ed-7dc4f79a6b40)
X [Pets of Space](https://ideas.lego.com/projects/e220707e-ea01-46f2-b853-6dca6c11b616)
X [NASA Space Shuttle & Launch Platform](https://ideas.lego.com/projects/350ba78e-7b5c-49d2-a343-19a99b3db70c)
**jdpennock**
X [UCS Space Shuttle](https://ideas.lego.com/projects/7bf51e13-ab09-45af-b3d7-89024d220ef7)
**thomasabeyer**
X [Voyager Spacecraft](https://ideas.lego.com/projects/17137290-d144-4e10-9fc9-a71433c20be5)
**JTlego3**
X [Space Shuttle](https://ideas.lego.com/projects/f377959c-c8e7-4716-b6da-ff10425a2cf8)
**8BitKid**
X [Microscale Space Shuttle](https://ideas.lego.com/projects/21152351-520e-44dc-8ab2-7928025b2ba8)
**chrismlarson**
X [ISS Base Set - International Space Station 1998-2007](https://ideas.lego.com/projects/d6949b63-fcba-4f29-ba4d-5e99bd2c6a60)
**diwhite**
X [Falcon 9 | SpaceX Rocket](https://ideas.lego.com/projects/561dcb67-938c-4f43-a5c6-467a7048cae1)
**Albinolan**
X [SpaceX BFR / Starship & Super Heavy 1:110 Scale](https://ideas.lego.com/projects/5199f8dc-34ad-492a-91e2-0008c8e37f81) - IN REVIEW
**MarsExplorer**
X [Asteroid Redirect Mission (A.R.M.)](https://ideas.lego.com/projects/cafcbacd-e7a7-4284-9652-4b1906645bd3)
**Space Creator**
X [New Horizons](https://ideas.lego.com/projects/7beac2c5-0cbd-48ff-8fbe-e6a3b1c87cfd)
X [New Horizons](https://ideas.lego.com/projects/55c805d2-9c71-4b7b-b3be-0a9973201684)
**PrimeBlder2319**
X [Mars Rovers](https://ideas.lego.com/projects/9fe2b3d7-179f-409f-9c49-dfb71415da74)
X [Mars Rovers](https://ideas.lego.com/projects/d6a58e05-f6cc-41e5-bd69-151d90ff0f62)
**Schrodingerman**
X [Dream Chaser](https://ideas.lego.com/projects/daa3ad97-893c-43f6-8c1c-b6b0fece17c2)
**percival74**
X [Soyuz Rocket](https://ideas.lego.com/projects/3ceda59f-1afb-43ee-8af9-4711e5a4ff50)
**DodgeDude5498**
X [SLS Orion (Saturn V Scale)](https://ideas.lego.com/projects/06ed2714-33f8-48f4-bae4-d8b9ca82f574)
X [Space Shuttle & Launch Pad](https://ideas.lego.com/projects/c578e9b4-61a4-4c2d-b61f-3684c8ca2f2d)
**whatsuptoday**
X [NASA Saturn-V Launch Umbilical Tower](https://ideas.lego.com/projects/a88109ec-9970-4fe1-98b4-9bd535856ab4)
X [NASA Space Habitat HERA](https://ideas.lego.com/projects/180302b4-8b44-422d-ad4c-9274b9633f06)
X [NASA SLS & Orion Spacecraft](https://ideas.lego.com/projects/14ff6fb6-5688-4fec-8cdb-4131724aa421), [MOCpages](http://www.moc-pages.com/moc.php/430932)
X [Mission Apollo 17](https://ideas.lego.com/projects/73cce907-875e-4386-9648-58e47eae480a), [MOCpages](http://www.moc-pages.com/moc.php/402610)
**Holy Sock Lord**
X [Falcon Heavy](https://ideas.lego.com/projects/950294d1-2465-4332-9c03-2205d55f8c33)
**spiarlgaia**
X [Galileo Satellite](https://ideas.lego.com/projects/2beb5056-7c03-409b-b7d4-77c3278a5d4c)
**Martin0131**
X [Ariane 5 \[1:110\]](https://ideas.lego.com/projects/ad87de58-ad4a-48fc-8962-1885a98521cf)
**Tony06**
X [European rocket Ariane 6](https://ideas.lego.com/projects/f5a3f90a-312c-4445-85ab-bad782b44eff)
**DavidLAPP**
X [The NASA Fermi Telescope](https://ideas.lego.com/projects/6b7075d8-c810-4d4a-a702-3270bcb8c655)
**reekardoo**
X [SPUTNIK I](https://ideas.lego.com/projects/370c4eb9-f5f3-4526-b8c7-362d38b53f30)
**DavidHiller**
X [First Flight](https://ideas.lego.com/projects/94694892-aaf3-40d9-8224-852d41ebda18)
**Emanuele_C_1094**
X [Rosetta](https://ideas.lego.com/projects/8249486c-5315-46d0-86c8-573398919f29)
**Vespone125**
X [Viking 1](https://ideas.lego.com/projects/9082b323-b07d-442c-a3e5-567d4d927f86)
X [Nasa Solar Dynamics Observatory (SDO)](https://ideas.lego.com/projects/dee10f9b-0a5b-492e-bc1e-20c804b6c613)
**korbenw**
X [HUGE ISS 1/28 (non definitive)](https://ideas.lego.com/projects/d6f884d2-8b72-4c8f-8a8c-51c81dcdc2aa)
**Alex_D**
X [Saturn V](https://ideas.lego.com/projects/803a57cc-c268-4ebb-976d-523ab5a6687f)
**HelloWorld1625**
X [Mars Orbiter "Odyssey"](https://ideas.lego.com/projects/0ff2b870-7a70-4102-9483-6cab293a7706)
**ikuzus**
X [Modular Soyuz](https://ideas.lego.com/projects/ecf68b76-ed83-4509-bf4a-7477e80a4351)
X [Modular Soyuz](https://ideas.lego.com/projects/b426abcf-5067-4f4e-ad2d-54d5e76fb50d)
X [Soviet Moon Landing](https://ideas.lego.com/projects/26d67967-270a-4eba-a3c7-0ec00c36f923)
X [Soyuz TMA-M](https://ideas.lego.com/projects/ef34245c-1345-4973-8268-2f62ccd9274d)
X [Soviet Moonshot](https://ideas.lego.com/projects/58ec666f-3648-42b3-b80f-d5fea9a8f99f)
X [Apollo Lunar Rover](https://ideas.lego.com/projects/91c3dca6-1c48-4fae-b08a-d776252e2edf)
X [Ranger Lunar Probe](https://ideas.lego.com/projects/9f054582-0daf-4930-ad78-b98339a275b7)
X [Miniature Skylab](https://ideas.lego.com/projects/08852668-7ba9-48fd-82fe-be175b74bd99)
X [N-1 Moon Rocket](https://ideas.lego.com/projects/063b8c6f-603d-471d-9341-6b18ef1f0003)
X [Minifigure Scale Mercury Spacecraft](https://ideas.lego.com/projects/c5614fb5-2824-422d-b3d7-d570f770f99a)
**smwjoat00**
X [Deploying the Hubble Telescope](https://ideas.lego.com/projects/62739162-4f2e-48dd-87d8-c32646ee8660)
**The Half Blood Baron**
X [Micro Soyuz Rocket](https://ideas.lego.com/projects/58d25d7e-c65a-4ad9-a678-9aa62353c6da)
X [Soyuz Capsule](https://ideas.lego.com/projects/4b601456-4588-41b5-90b1-7571230022ec)
X [Soyuz Rocket](https://ideas.lego.com/projects/d4fce4ec-4b68-4f7e-b85b-e1b628edc684)
X [Desktop Soyuz Rocket](https://ideas.lego.com/projects/e5d7bf98-18d1-4c9e-a7ef-34bb5e0ba482)
**Aerospace Engineer**
X [Project Mercury, Mercury Redstone MR-3](https://ideas.lego.com/projects/537811a6-f4ff-448d-9c24-c13f3874fa23)
**Astro-olives**
X [Rosetta – Philae Mission](https://ideas.lego.com/projects/c1d7db40-43a6-432b-95b4-e05a443923dc)
X [1:110 NASA Space Launch System Block 1](https://ideas.lego.com/projects/959b9b78-f67e-48e5-9e63-89a848e6c214)
X [1:110 NASA Gemini Titan II](https://ideas.lego.com/projects/9ebe46b2-4d20-4677-af90-10b0ba8c520d)
X [Voyager 1](https://ideas.lego.com/projects/3ffd2032-587e-4017-b636-ccb87db51f52)
X [Mars Pathfinder](https://ideas.lego.com/projects/6e3f6089-a8bf-4db9-a67f-2b389fdaff51)
X [ISEE-3 Space probe](https://ideas.lego.com/projects/783038ac-2bfd-453b-9f5e-66c5b016a1e4)
**MattRepo2000**
X [Voyager 1](https://ideas.lego.com/projects/c6792d1f-5c7f-40da-a988-8b082ac5feb5)
**BrickGallery**
X [Juno](https://ideas.lego.com/projects/7814672c-9b23-4c0e-b001-8a4e68e7b6d4)
**Captain America**
X [Block.1 SLS Rocket](https://ideas.lego.com/projects/15553ad4-f5ee-449d-80b3-3fc92d2d0703)
**LuisPG**
X [Voyager](https://ideas.lego.com/projects/5ef4dade-8d6d-4a9a-addd-f3e4a6415e8c)
X [ALMA Observatory Antenna & Black Hole](https://ideas.lego.com/projects/9f1be91e-a7bb-47af-a02c-f93396a45b99)
X [Hubble Space Telescope](https://ideas.lego.com/projects/2ae12e53-773c-42ed-86b6-3ae53367f0a2)
X [Soyuz Spacecraft](https://ideas.lego.com/projects/7d3cbb83-4f79-4469-98c9-d54ea4dd17e8)
X [Tesla Roadster Starman](https://ideas.lego.com/projects/de38f50a-3bc2-43bc-9ef1-831f5587cc6a)
X [Voyager interstellar mission](https://ideas.lego.com/projects/7c96f2b8-62e3-45f5-9be5-83d25a0190cf)
X [Sputnik 1](https://ideas.lego.com/projects/ebe03373-a332-4204-a1c3-28af7d3c5b7f)
X [Return of Apollo 11](https://ideas.lego.com/projects/a6b0b909-ff72-4ec2-b2cf-960b68fc98bd)
X [Lunar Rover 1971](https://ideas.lego.com/projects/db27530a-267e-4e88-a8d1-d10333bed611)
X [Sputnik 1957](https://ideas.lego.com/projects/56bb68b3-7471-4439-9ada-aeffd82952a0)
X [Apollo-Soyuz Mission](https://ideas.lego.com/projects/1dac6e69-af75-4515-9015-458e6352373e)
X [Eagle Has Landed](https://ideas.lego.com/projects/c1286051-a62d-4fc7-9a45-08a088f38cae)
X [Cape Canaveral Spaceport](https://ideas.lego.com/projects/f842a83a-9cc9-4cd5-a383-82b3d3e3e73c)
X [Soyuz Spacecraft and Rocket](https://ideas.lego.com/projects/d491af50-9b1a-4b42-abae-ffe0f1265ceb)
X [Saturn V](https://ideas.lego.com/projects/f4626a5f-34c6-49e9-8c32-a3a6dec3904c)
X [Apollo 11](https://ideas.lego.com/projects/05abaa50-b17b-4192-9ae0-491cd182b295), [MOCpages](http://www.moc-pages.com/moc.php/400183)
X [Apollo 11 Rescue Mission](https://ideas.lego.com/projects/0d37b842-5d11-476b-bb37-1bf659c9efdf), [MOCpages](http://www.moc-pages.com/moc.php/401299)
X [Orion Spacecraft Test Flight](https://ideas.lego.com/projects/81bc26b0-555e-4d5d-8d51-8913acdc0cdc), [MOCpages](http://www.moc-pages.com/moc.php/401811)
X [Saturn IB Rocket](https://ideas.lego.com/projects/961d43c6-5c9d-4d03-820b-af2240a3f2f2)
X [Soyuz-U Rocket](https://ideas.lego.com/projects/20bf2c53-2c35-46c2-81b8-b164a8c2aae7)
X [Lunar Rover](https://ideas.lego.com/projects/60a186b6-8df7-48e0-823a-e169e24cb55b), [MOCpages](http://www.moc-pages.com/moc.php/404709)
X [Apollo-Soyuz Mission 1975 (minifig scale)](https://ideas.lego.com/projects/b9748ffe-80e4-462a-8f7b-dd99d26de993)
X [Dawn Spacecraft](https://ideas.lego.com/projects/9df37c63-9780-41ee-a599-42d73f69dffb)
X [New Horizons & Pluto](https://ideas.lego.com/projects/7cfd1330-6318-4d1a-8ea5-1908aec43eab)
X [Air & Space Museum: Space Exploration Collection](https://ideas.lego.com/projects/31a0f433-df83-4e28-9e7f-2b4e68f2c364), [MOCpages](http://www.moc-pages.com/moc.php/421396)
X [Soyuz](https://ideas.lego.com/projects/10b032e6-00ec-45f8-90d6-f4c40891fe71)
X [Cassini-Huygens Spacecraft](https://ideas.lego.com/projects/8e2b68f2-83a8-4603-aa32-8bf18a9cd5ec)
X [Voyager](https://ideas.lego.com/projects/c5a0c699-b5ae-4315-a57d-33857d6923a3), [MOCpages](http://www.moc-pages.com/moc.php/400206)
X [Apollo Program Moon Explorers](https://ideas.lego.com/projects/22259789-f741-40f4-b48f-118ce9af2774), [MOCpages](http://www.moc-pages.com/moc.php/429986)
X [Apollo 13](https://ideas.lego.com/projects/1296de82-de70-4bed-bdeb-e2ded8f78c8f), [MOCpages](http://www.moc-pages.com/moc.php/422360)
X [Minifigure: Air and Space Museum](https://ideas.lego.com/challenges/0eded380-a6bd-402c-be30-2b9bac94c1cc/application/b1399615-307e-4f21-80f1-fa52f934b099)
**MissionPatch**
X [OSIRIS-REx Asteroid Sample Return Mission](https://ideas.lego.com/projects/c7771c06-45b2-47f5-8bfb-22ca47b789da)
X [Atlas V Rocket](https://ideas.lego.com/projects/d756ad75-9e24-48f0-9d81-bcd87212a5db)
**Mr_Finkey**
X [Mercury Rocket](https://ideas.lego.com/projects/a4902822-7f24-499c-9f49-ed88155b086a)
**GoodOlives**
X [Rosetta Satellite w/ Philae comet lander and 67p comet](https://ideas.lego.com/projects/dc2f1dac-b1db-4e09-981b-248e61665b3f)
X [Cassini-Huygens, Saturn probe](https://ideas.lego.com/projects/578563f3-8e9a-44f2-8fe6-ec10ea6ff8e4)
X [Mars Pathfinder and Sojourner](https://ideas.lego.com/projects/ea910267-df36-4ed2-a8bc-f5ae6f878b7e)
X [Spirit/Opportunity Mars Rover](https://ideas.lego.com/projects/ff4fcf3f-90c5-4808-b4ef-53106a317abd)
X [Mercury Freedom 7 Capsule](https://ideas.lego.com/projects/6273ab62-08ab-475c-a93c-28a7ddf4ec8a)
X [Soyuz TMA-6](https://ideas.lego.com/projects/ce3603be-c4fa-4540-9f35-b621c5081724)
X [Juno Spacecraft](https://ideas.lego.com/projects/074974d6-bac8-4a69-8014-5d41d747e8d7)
**Legless_Lego_Legolas**
X [Lego Soyuz Capsule](https://ideas.lego.com/projects/fbe51baf-edce-4bf7-9a38-59d63aa018f1)
**BlizzardBlazer**
X [NASA Vehicle Assembly Building (1:900)](https://ideas.lego.com/projects/6885f2c2-4679-4bdd-a8ce-6d6de852b808)
X [The Complete Saga](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/746c5c73-60c5-4fb6-a1a0-974bbfae0022)
**Stevenhoward27**
X [Mini-fig Scale Project Gemini](https://ideas.lego.com/projects/473e666e-0c05-4cd5-9a3c-fcde78e09e7e)
X [Soyuz Rocket](https://ideas.lego.com/projects/7a3be173-1a90-4941-b171-5531ee59d6f3)
**Jeffy-O**
X [Canadarm](https://ideas.lego.com/projects/bfcfd330-cb30-4f25-9c0d-b5aeb63ee458)
**saabfan**
X [The Rosetta Mission](https://ideas.lego.com/projects/6b68cdfa-e703-4200-9097-d57aeb977e76)
**QwertyLD**
X [NASA Lunar Orbital Platform - Gateway](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/51ffe9fe-b1a1-4616-9672-f36f7a850f43)
**Chirrutomwe99**
X [SLS Rocket with Orion Spacecraft](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/f2c137d6-c0cf-4b93-99fe-d8e4c925a33a)
X [The Orion Spacecraft](https://ideas.lego.com/projects/146098fd-6e67-4c1e-959a-63b5efbc088d)
**Yellowbrick350**
X [The Apollo program](https://ideas.lego.com/challenges/99a66402-19d3-4c70-a631-2d56b59a062b/application/22d153d0-acdb-44ee-8607-36e587ac306a)
**Jeno1**
X [Airstream NASA Astrovan](https://ideas.lego.com/projects/d9f5e369-1341-4e20-8928-e45dd0965c1b)
**plutofan**
X [New Horizons spacecraft - Pluto-Kuiper Belt mission](https://ideas.lego.com/projects/e5cd3421-ebbc-4abc-9c6c-2a298dddc936)
**StarHuman**
X [SpaceX Big Falcon Rocket to Mars](https://ideas.lego.com/projects/38e2c293-7d94-4090-80ad-b881ecc3732a)
**JDL1321**
X [SpaceX Falcon 1](https://ideas.lego.com/projects/b9408d1f-2022-461a-88ea-1d97710deb04)
**spider689**
X [James Webb Space Telescope](https://ideas.lego.com/projects/901235b7-78df-4200-97da-d9bd54922140)
**CARLIERTI**
X [Lego Space Launch System](https://ideas.lego.com/projects/99c052b3-2183-4dd6-b1a0-cc1b1f121f30)
**JackChalk6265**
X [Blue Origin New Shepard System](https://ideas.lego.com/projects/47767d02-b68a-4e49-8fad-c97998576da1)
X [Space X Falcon 9 Ultimate Collection](https://ideas.lego.com/projects/298a2026-2652-44e7-9c20-76c34a84b811)
X [Space X Cargo Dragon](https://ideas.lego.com/projects/84908b18-5fdc-4096-9c57-b09dadacf6e9)
X [Space X Dragon](https://ideas.lego.com/projects/343191dc-0edd-4d8d-a852-60e0e4b2af89)
X [Space X Falcon 1 (Two Scales)](https://ideas.lego.com/projects/7b622452-d1ce-4bc4-9855-4e3ae0740b43)
**Mastertiti74**
X [The Hubble Space Telescope](https://ideas.lego.com/projects/8bad986f-e7da-4194-a1e0-b74857b39f71)
**TravisKirby**
X [NASA Space Shuttle Discovery](https://ideas.lego.com/projects/6ff8b4c8-7a76-430f-bed2-27f0b9ea6d9b)
**orvman**
X [Mini Space Shuttle](https://ideas.lego.com/projects/b0df7121-d66f-485b-9b23-aae4bc04185f)
**BKT722**
X [Juno Spacecraft](https://ideas.lego.com/projects/dc84ac19-2b1f-4ab4-9d3b-26eb76e075ac)
**Ogisama**
X [NASA Space Transportation System](https://ideas.lego.com/projects/b91a3dd4-7716-4513-bad3-b0502af3436f)
X [Space Shuttle Orbiter with Hubble Space Telescope](https://ideas.lego.com/projects/aaf8c154-fc76-4ead-ab32-6646d3c18385) - instructions will be shared if asked
**Legoboy9247**
X [International Space Station](https://ideas.lego.com/projects/f6ca60b2-c478-4a39-90a3-c95ce0b19134)
**redbigsi**
X [International Space Station](https://ideas.lego.com/projects/ad63f7c5-2679-4d75-821f-b6d3d26a1331)
X [Cassini Huygens Spacecraft](https://ideas.lego.com/projects/b7d0b579-c4d3-4eae-8e82-4419b7f7bf5b)
**Sandal**
X [Vostok-1: The First Spacecraft](https://ideas.lego.com/projects/a73cfd82-49d4-41df-930b-7a5113e4d462)
X [Space Shuttle Discovery](https://ideas.lego.com/projects/d8ff24ce-7ee5-4a8f-aac8-7a65601160c2)
**SuperSpeedShuttleD**
X [Space Launch System](https://ideas.lego.com/projects/2290a032-5bd7-4602-9d72-eb0e1210c691)
**sjogerst**
X [SpaceX Falcon 9R](https://ideas.lego.com/projects/1bb8ee3e-6c9a-4891-9e3b-8b52abb7e633)
**SquidHunterZ1**
X [Vostok 1:- The First Human in Space](https://ideas.lego.com/projects/28b7b451-7ac3-4029-add6-af42a6c4f947)
X [NASA Pioneering Rocketry:- The First Humans in Space](https://ideas.lego.com/projects/ac5f5858-2b01-48ef-a464-3fbcf0302167)
X [N1 Rocket with Launchpad](https://ideas.lego.com/projects/84b8f65a-47bc-40d4-9701-627e3230cf6e)
**daisuke**
X [Space Shuttle : Atlantis](https://ideas.lego.com/projects/0e2a6eba-b3a8-4be7-821c-7e19d192cd55)
X [Venus Climate Orbiter AKATSUKI](https://ideas.lego.com/projects/23de2540-e9e2-4343-8574-92f69dd35a05)
**RICTONG**
X [Micro space shuttle](https://ideas.lego.com/projects/dadc6136-953d-40f0-9aa2-a6feab3b9e35)
**SquidTron9551**
X [Hubble Space Telescope](https://ideas.lego.com/projects/f0e2dece-baf6-4033-abc5-d226e7e796be)
**Yukon**
X [Space X Mr. Steven](https://ideas.lego.com/projects/f060370b-ac2e-4c57-bd40-4f8dddfee427)
X [Space X Falcon Heavy](https://ideas.lego.com/projects/187b31dd-2d54-4bc4-939f-127a883a279f)
**adpvision15**
X [Spacex Falcons](https://ideas.lego.com/projects/3241f357-05d4-4a39-bd6e-e1c8a6b64cad)
**Maverick9154**
X [Hubble Space Telescope](https://ideas.lego.com/projects/c927154b-1d60-48c9-8cc4-8b615a901d6f)
**Snelson42**
X [UCS Space Shuttle Atlantis](https://ideas.lego.com/projects/f072bc4c-47a3-417e-98cb-5750fdac516d) - IN REVIEW
X [X-37b Space Plane](https://ideas.lego.com/projects/07ad0885-2060-4d5d-bee9-3cfd3b338924)
**JediBob2**
X [Ultimate Collector's Edition Voyager 2 Space Probe](https://ideas.lego.com/projects/84e3c510-0963-4652-8915-0c7ea0437f75)
**smileyguy**
X [Lego Space Launch System Block 1 Rocket](https://ideas.lego.com/projects/7a54dbcb-f56b-4343-8571-8c660a701f40)
**M9kid**
X [SLS Block 1 (70- Metric-Ton) Space Shuttle](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/73a476ec-b985-4e38-8c3b-7552a6877a79)
**MattHouTx**
X [Falcon Heavy rocket](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/b43d1603-a0ff-4ea9-b44f-1f4d5484b6a2)
X [SpaceX Falcon Heavy](https://ideas.lego.com/projects/1e3c9c84-840b-4735-9658-e1c8d8ac4f9d)
X [SpaceX Falcon 9](https://ideas.lego.com/projects/4a4ee28a-f70e-48e0-866b-e95d03ea6190)
**Kingfisher_007**
X [James Webb Space Telescope (Articulated)](https://ideas.lego.com/projects/05fb57ff-a8d9-4795-8408-32d9e2114dd1)
**Ponpanpino**
X [SpaceX - Falcon 9](https://ideas.lego.com/projects/6ff740ee-ba46-4142-99ee-ca732c341105)
**Brianr8811**
X [NASA Deep Space Habitat and Rover](https://ideas.lego.com/projects/e38ef75b-59e5-4cd4-85c3-44266db1938a) - also on [Eurobricks](https://www.eurobricks.com/forum/index.php?/forums/topic/67164-nasa-deep-space-habitat-model-and-rover/&tab=comments#comment-1227158)
**OwenBrendan**
X [Motorized Space Shuttle Transporter](https://ideas.lego.com/projects/8016bdfc-5e3c-40ea-9fe6-3b8da7e9a6b5)
**Jimmi-DK**
X [Sojourner - The Little Mars Rover](https://ideas.lego.com/projects/10f51034-2591-4bcd-b568-2735fd84a5be)
X [SpaceShipOne](https://ideas.lego.com/projects/6c077c4e-9ece-4e82-9690-3edbad2784ee)
**Delusion Keeper**
X [The Edge of Space: Space Jump World Record 2012](https://ideas.lego.com/projects/1e8ee853-c9ed-43ea-8cea-74b027ebc127)
**Oldschool74**
X [Falcon Heavy Mars Mission](https://ideas.lego.com/projects/5f298682-85ef-426b-91bf-595ad6209d3f)
**rovio7**
X [Mini Space Shuttle](https://ideas.lego.com/projects/77acbd35-9271-4cbe-b008-718cdf039c1a)
**legospacex5**
X [Spacex Falcon 9 Rocket](https://ideas.lego.com/projects/0ba1c0dc-3586-459a-a5ed-d0cdd6ddabe5)
**SpaceRage**
X [NASA Space Launch System (Block I)](https://ideas.lego.com/projects/d9689406-eaad-435b-9adf-1cc3898a5eaf)
X [SpaceX Falcon Heavy](https://ideas.lego.com/projects/a2ae3e98-2604-4b6a-b087-2fe39b61c374)
**rhino6810**
X [International Space Station (Micro)](https://ideas.lego.com/projects/2719cb46-f5f4-4bda-8314-65a7c4f58805)
**ThomasW**
X [ESA Rosetta Mission - Lego Space History (Including Philae and 67P/Churyumov–Gerasimenko)](https://ideas.lego.com/projects/a696ac38-7ace-47af-961f-b3e598738a13)
X [ESA Rosetta Mission - Lego Space History (Including Philae and 67P/Churyumov–Gerasimenko)](https://ideas.lego.com/projects/6d454538-f219-4239-b2e8-3518d7009f85)
X [Lunar Rover (LRV - Lunar Roving Vehicle)](https://ideas.lego.com/projects/6919c7cf-a750-48d9-88b1-fd4426317238)
**Surox**
X [Mini NASA Hubble Space Telescope](https://ideas.lego.com/projects/d838b62c-00e2-4700-a519-062e4a179a8c)
**The Cult Of Skaro**
X [Space Shuttle](https://ideas.lego.com/projects/82a68bf0-abf7-46d9-9d48-da4550db48f8)
**SOBoyle1**
X [Ares 1 Rocket](https://ideas.lego.com/projects/75d15a4a-8422-42ee-81cc-9c5918b9db03)
X [Soyuz - Rusian Space Race Rocket](https://ideas.lego.com/projects/3d70281f-346b-4494-92a0-bab2e5f36eeb)
**Teazza**
X [Space Shuttle Launch - microscale](https://ideas.lego.com/projects/3e431fe0-cc30-4926-8545-90dceab5681d)
X [Architecture: Space Shuttle Launch Complex in nanoscale](https://ideas.lego.com/projects/3e2cc519-aa0c-4ca8-ae05-4cb7af6dc540)
X [Architecture: Space Shuttle Launch Complex 39](https://ideas.lego.com/projects/a89d5c41-1253-4d8b-9475-b7f1b0130ada)
X [Architecture : Space Shuttle crawler transporter](https://ideas.lego.com/projects/80678b42-d645-48af-9445-d41a00d5d84b)
X [Space Shuttle Launch Complex](https://ideas.lego.com/projects/b150960f-e0cf-47e8-9bb6-0c2e1f11f66d)
**AdamSenna**
X [Space Shuttle with tank and boosters](https://ideas.lego.com/projects/2e40b35e-eb5b-4d35-812e-febbef8da84b)
**GalileoFOC**
X [Galileo FOC Spacecraft](https://ideas.lego.com/projects/271a988d-925d-44ee-98ad-4ab2c52d3a13/updates#content_nav_tabs)
**Anscott2**
X [Boeing 747 and Space Shuttle](https://ideas.lego.com/projects/5c3a90cc-57d6-4788-b85b-0c69f17df4ba)
**jelletv**
X [Orion space schip.](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/6bb11aba-e4f2-408e-8749-2e9ccb700ad3)
**Ilandovprais**
X [International Space Station®](https://ideas.lego.com/projects/f1932810-70e1-4c5c-b147-89c0da7f8fc1)
**AlanGuerzoni**
X [Top Gear Reliant Robin Space Shuttle](https://ideas.lego.com/projects/a531ede3-3e2c-43c4-b162-cd69e1d7dbc7)
**Cidades De Blocos**
X [The Hubble Space Telescope](https://ideas.lego.com/projects/8e8e27aa-bcb6-4327-9018-4cd00b4134d1)
**BadPirate**
X [SpaceIL - Spacecraft Beresheet The Journey to the Moon.](https://ideas.lego.com/projects/7068c074-c55b-4a06-8192-518206cf321f)
**TheDoveDepot**
X [Voyager 1 - NASA's Space Probe](https://ideas.lego.com/projects/ddc19f25-36a5-4f20-a59a-94ae6e21dbad)
**MinifigInSpace**
X [Classic Space Golden Explorer](https://ideas.lego.com/projects/2aec18b2-e0d7-43f6-b101-7920e55e86cc)
**StrikeEagle**
X [Space Launch Delivery System (SLDS)](https://ideas.lego.com/challenges/a289cd61-c960-4364-a1a8-42da645a4fe6/application/81b8cabe-057c-4658-b5b6-477990f93b1a)
**Micro_Model_Maker**
X [Nasa Spacecraft](https://ideas.lego.com/projects/e59079f3-b522-46ed-b790-e29a42f88038) - IN REVIEW, also on [Eurobricks](https://www.eurobricks.com/forum/index.php?/forums/topic/152113-moc-nasa-spacecraft-and-missions/)
**XCLD**
X [MIR Space Station](https://ideas.lego.com/projects/2c0c06b4-c277-457f-ac68-37420867dda4)
X [Hubble Space Telescope](https://ideas.lego.com/projects/f4f14363-2ede-4062-ae10-679ed3e55a77)
X [Shuttle Carrier Aircraft](https://ideas.lego.com/projects/db82fd2b-65a6-4878-b000-67ae73a8a92b)
X [Soyuz Rocket with Spacecraft](https://ideas.lego.com/projects/6642e725-0627-43b5-8258-af0f1fd1f4cd)
X [International Space Station (Architecture Edition)](https://ideas.lego.com/projects/2d80a2f6-9442-42af-8923-12e30e45c729)
X [Baikonur Cosmodrome Launchpad (Architecture Edition)](https://ideas.lego.com/projects/5607e96f-8ec7-46ed-8335-3097e83a2822)
X [Baikonur Cosmodrome Launchpad](https://ideas.lego.com/projects/6abf6330-88ca-4321-8632-3026747af026)
X [International Space Station](https://ideas.lego.com/projects/571a5261-35f5-43db-bb84-2cc3ae6786ff) - NOT APPROVED
X [Vega 2 Space Probe](https://ideas.lego.com/projects/2bc24a89-b8cb-4351-a0d4-c8290556c851)
X [Mini-ISS (Desktop Edition)](https://ideas.lego.com/projects/ab95596f-d216-4e66-9d2c-a4156b58e926)
X [International Space Station](https://ideas.lego.com/projects/51953742-74f5-42b9-84c7-ecf291bf2d87) - NOT APPROVED
**Ethan3**
X [Space IL Beresheet](https://ideas.lego.com/projects/0a395d1e-04c9-47a8-8b1e-f89c2a82027c)
**SomeLight studio**
X [Vostok 1——The First Ever Manned Spaceflight](https://ideas.lego.com/projects/3666468a-1f16-4fc4-a87a-fd98a88fee94)
**MartinOt**
X [around the Earth's orbit](https://ideas.lego.com/challenges/1b817aba-3990-4e6d-a17f-7a59a948d02f/application/27fa0e08-2d7b-4050-b91c-db0dc2789896)
X [Living in space](https://ideas.lego.com/challenges/1b817aba-3990-4e6d-a17f-7a59a948d02f/application/57d9fe7a-1481-471d-b0f9-be89439ec644)
X [Is there life on Mars?](https://ideas.lego.com/challenges/1b817aba-3990-4e6d-a17f-7a59a948d02f/application/9f19e48d-cd35-4e75-afbe-ee49d5960d90)
**haymaw99**
X [SpaceX Starship and Super Heavy (BFR)](https://ideas.lego.com/projects/641c0400-624b-495d-93d6-6b43299f1f5e)
**Krohit**
X [Chandrayaan-2: Moon Exploration](https://ideas.lego.com/projects/71488381-6ed0-459b-93c0-7fa29d0189f8)
**Michael0071**
X [Mars Probes and Satellites](https://ideas.lego.com/projects/485c5369-bdee-4810-9bfc-cc2c3939e76e)
**JTS Pilot**
X [Voyager 1 American Space Probe](https://ideas.lego.com/projects/e69064e7-b0bb-4101-951d-203a5de29f44)
**coolguyiguess**
X [Voskhod 1](https://ideas.lego.com/projects/d12c246f-61d8-41fe-bc46-72ec9ae35d45)
**Stevenhoward27**
X [Mini-fig Scale Project Gemini](https://ideas.lego.com/projects/473e666e-0c05-4cd5-9a3c-fcde78e09e7e)
**Geroditus**
X [Outer Solar System Explorers](https://ideas.lego.com/projects/80e3c12d-4628-470e-873a-5cc73b46d5c7)
**Andrijan94**
X [James Webb Space Telescope](https://ideas.lego.com/projects/9353f9ab-a11f-4820-8e1c-c427f7b17bbc)
**joelsprunger**
X [Mission Control - Apollo Missions](https://ideas.lego.com/projects/ce797589-20ac-4a4c-ab22-b3b54a844246)
**BooCrackers12**
X [SpaceX Dragon 1](https://ideas.lego.com/projects/8f409ed8-da0a-4f81-b745-af184deed5c9)
X [Blue Origin - Ultimate Collection](https://ideas.lego.com/projects/f894c29b-2851-4b3a-b755-78fba9531f41)
X [ISA Spacecrafts](https://ideas.lego.com/projects/a3b77ce8-199b-41e4-a633-c4a1ef3df33b)
X [Minifigure Scale Project Mercury](https://ideas.lego.com/projects/f51945c7-5c6c-4af4-9601-33afa0e75263)
**LEGOANDREW1**
X [Skylab](https://ideas.lego.com/projects/731dac44-b486-4baf-a829-4b126c5f7bfa)
**SecondTelescopicBean**
X [NASA VIPER Moon Rover](https://ideas.lego.com/projects/8a754109-6b45-4fd1-8372-28f3435e90f8)
**TheCreatorOfLEGO**
X [Nasa SLS Artemis](https://ideas.lego.com/projects/503768fc-a478-409e-ad86-8c983cd2e3f4)
X [Falcon 9 Block-5 Crew Dragon](https://ideas.lego.com/projects/fdbd2c68-449d-402c-9593-bb6b20213532)
X [SpaceX Falcon Heavy](https://ideas.lego.com/projects/a68a34c9-1f73-4f50-a97e-7e277edab9c9)
**snilloc25**
X [NASA Mars Helicopter Ingenuity](https://ideas.lego.com/projects/e6b785f9-2350-44ea-a177-78bb02445e76)
**Der17**
X [SpaceX Demo 2 Mission with Crew Dragon 2 & Falcon 9 Block 5](https://ideas.lego.com/projects/c625b6aa-d338-4cf5-a50d-5c48bb8ad61d)
**CaptainJC95**
X [Space X Dragon Collectors Set](https://ideas.lego.com/projects/e55c4b12-8bb9-458a-9c1d-3c0fbb4c219b)
X [Launch Pad 39A: Space X Falcon 9](https://ideas.lego.com/projects/addaa1eb-aa64-438c-9916-365425b38c44)
**ARTILIFE**
X [SpaceX Crew Dragon Spacecraft](https://ideas.lego.com/projects/5116ba93-0e2a-45da-aa8f-80633d792ab7)
**Yomi_02**
X [SpaceX Falcon 9, Crew Dragon and OfCourseIStillLoveYou](https://ideas.lego.com/projects/1f6d06dd-73f5-42fd-81d3-4d3b13fbb38f)
**Naunau**
X [Falcon 9 Crew Dragon Launch](https://ideas.lego.com/projects/8afa3392-f304-4698-9ec0-b5178b18920e)
**H_BRiCK**
X [SpaceX Falcon 9](https://ideas.lego.com/projects/a8d14a6b-07e9-4a28-84f8-267fd8964000/updates#content_nav_tabs)
**THEguy342**
X [SpaceX Starship](https://ideas.lego.com/projects/5ea6e633-d7ea-4536-b89d-be16b4595c78)
**Bob-R**
X [Starchaser Industries' Nova 2 Rocket](https://ideas.lego.com/projects/d098bb4a-2b99-4afe-bbbe-15f77294a867)
**steveiuliano**
X [SpaceX Starship - on Mars](https://ideas.lego.com/projects/f1cda019-d668-4d6e-bcd5-7b2057e07369)
**BooCrackers12**
X [SpaceX Dragon Endeavor (Demo-2)](https://ideas.lego.com/projects/bb7bb1d4-5705-48c1-8f88-b5d54a8e2cac)
**Marioo8**
X [Voyager 1](https://ideas.lego.com/projects/b0763218-a1f7-42a7-b57f-3c6c9248edc9)
**Spacemanship**
X [NASA Artemis Space Suit](https://ideas.lego.com/projects/4b24ba08-2d51-4709-80c2-3469be59c292/updates#content_nav_tabs)
**legotruman**
X [Apollo 11 Spacesuit](https://ideas.lego.com/projects/b667ff22-778d-4efb-b62b-7dd140823866)
**FSLeinad**
X [Mini ISS](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/86dc0d6f-3a25-46d4-8a71-e98af76d7f06)
**Plutonian1**
X [Mars Science Laboratory Curiosity Rover](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/34d36ded-2f73-4b62-aa77-ad27589adc1f)
**The Brick Biker**
X [International Space Station](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/cf09e82b-f101-4b28-9565-af7697dbe2a0)
**hugo_tby**
X [Lego Curiosity Rover](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/bae8ff58-6e11-4d3c-bc6d-70d5bd3bd3de)
**Piercyn**
X [International Space Station](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/1110ef66-84b2-459b-afb2-cc9da03594f3)
**Brick_Christian**
X [Mini Iss Ideas!](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/424d3af6-e161-42fe-8220-9bfb04f22557)
**dirkman**
X [Women of Science micro-build](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/b7b5a0b5-79cc-4309-8eb0-219ea6cfe8ac)
**AddictStudios**
X [Saturn V](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/49b6d1e0-0fdb-4086-bb66-ff6f03f9fda2)
**SpeedHun**
X [Microscale Hayabusa Spacecraft](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/cf96bdf4-b746-4d8e-a22d-7df17d19e277)
**The Brick Biker**
X [Saturn V Rocket](https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/e3c88829-8b59-465b-8f0e-611c4c92a3f3)
**Serge_Pooh**
X [Atlas 5 rocket (1:110)](https://ideas.lego.com/projects/10cbd9c9-7447-406f-8918-d120e06b25ea/)
X [SpaceX Starship](https://ideas.lego.com/projects/35f20a1d-d2bd-41b8-ad42-53be6eb9a3a3)
**SpaceLegoFan1**
X [Juno (Jupiter Orbiter)](https://ideas.lego.com/projects/a9fcc610-2e25-426e-977e-eb6cfdd1d4c1)
**Marioo8**
X [Delta IV Heavy](https://ideas.lego.com/projects/a07006ce-330d-4605-ab4d-ea788daa2b59)
**ExoMartian**
X [ExoMars Rover "Rosalind Franklin"](https://ideas.lego.com/projects/4b21a910-3cb3-45b4-8c67-180356ffa5a9)
**jasonrfox**
X [NASA Voyager 2 and Buildable Gas Giant Planets](https://ideas.lego.com/projects/73dd9ed0-5e37-4b07-8708-f178f196a5b3)
**TheBrickSpace**
X [60 Years of Human Spaceflight Rocket Garden](https://ideas.lego.com/projects/c7521673-fdce-4b61-9e0b-3e6e0cad0613)
**derfel72**
X [NASA Astrovan](https://ideas.lego.com/projects/9d723c57-183c-4938-ab16-72c1de2acdd4)
**BriocheRockets**
X [NASA's Mercury-Redstone Rocket](https://ideas.lego.com/projects/4907be03-5383-4da0-b110-44f3395f4b02)
**AreospaceAndPlane**
X [Space X Falcon Heavy](https://ideas.lego.com/projects/c296a7ea-c099-4143-b29f-4fd57a702a86)
**Vinc77**
X [MISSION SPUTNIK (1and2)/ the First Satellite](https://ideas.lego.com/projects/acf8b151-1cca-4696-8539-f173aace9f2d)
**Building Puffin**
X [The Electron Rocket From Rocket Lab](https://ideas.lego.com/projects/b8a9dc5f-6397-4fcd-abf1-cf847ec27f6a)
**Spacemanship**
X [Brickheadz Astronautz: SpaceX Spacesuit](https://ideas.lego.com/projects/e924bcf2-12d9-48cc-b0d4-d02fdbdef8bc/updates#content_nav_tabs)
X [Brickheadz Astronautz: NASA Spacesuits](https://ideas.lego.com/projects/24bb475f-1a46-4601-b55b-b50826aa30ec)

## Projects found on Eurobricks
X [MOC: LDD Soyuz-2 Rocket By tl8](https://www.eurobricks.com/forum/index.php?/forums/topic/66253-moc-ldd-soyuz-2-rocket/&tab=comments#comment-1204475) - there are instructions, but cannot be downloaded
X [Soyuz TMA-9 / FG Rocket By Henchmen4Hire](https://www.eurobricks.com/forum/index.php?/forums/topic/66303-soyuz-tma-9-fg-rocket/&tab=comments#comment-1205644)
X [ MOD Apollo 11 Lunar Lander 10266 By LemFliggity](https://www.eurobricks.com/forum/index.php?/forums/topic/173051-mod-apollo-11-lunar-lander-10266/)
X [Saturn V Launch Pad with Umbilical Tower by teflon](https://www.eurobricks.com/forum/index.php?/forums/topic/172263-saturn-v-launch-pad-with-umbilical-tower/&tab=comments#comment-3124076), [Bricksafe](https://www.bricksafe.com/pages/teflon/saturn-v-technic-launchpad)
X [Space Shuttle Launch Complex 1:110 scale](https://www.eurobricks.com/forum/index.php?/forums/topic/175615-space-shuttle-launch-complex-1110-scale)
X [MOD Apollo 11 Lunar Lander 10266 By LemFliggity](https://www.eurobricks.com/forum/index.php?/forums/topic/173051-mod-apollo-11-lunar-lander-10266/)
general satelite [curtisdf - Roche Comm Satellite](https://www.bricklink.com/v3/studio/design.page?idModel=33437)
general satelite [curtisdf - #3368 Satellite Payload](https://www.bricklink.com/v3/studio/design.page?idModel=33153)
X [[WIP] Space Shuttle By Jeroen Otten](https://www.eurobricks.com/forum/index.php?/forums/topic/175791-wip-space-shuttle/)

## Projects found on Bricklink
X [endrega - SpaceX Falcon Heavy 1:110 Scale with Scale Tesla Roadster](https://www.bricklink.com/v3/studio/design.page?idModel=32500) - no instructions because outdated.
X [Viking2003 - Soyuz Module](https://www.bricklink.com/v3/studio/design.page?idModel=89845)
X [Xael - Falcon 9 Block V Sooty First Stage](https://www.bricklink.com/v3/studio/design.page?idModel=70956)

## Projects found on MOC pages
2DO all MOC pages
X [Commander Bricks - 2015 MocAthalon:Hard boiled potatoes Hubble](http://www.moc-pages.com/moc.php/408091)
X [Locutus 666 - Space Shuttle 1/350](http://www.moc-pages.com/moc.php/386378)
X [Locutus 666 - ISS and Space Shuttle 1/700](http://www.moc-pages.com/moc.php/396599)
X [Kyle Peckham - 2012 MocAthalon: Challenger: A Day To Remember](http://www.moc-pages.com/moc.php/315927)
X [Kyle Peckham - 2015 MocAthalon - Venera 7](http://www.moc-pages.com/moc.php/408332)
X [Kyle Peckham - Space Shuttle Discovery](http://www.moc-pages.com/moc.php/362672)
X [Travis Kirby - LEGO Cuusoo Space Shuttle Project](http://www.moc-pages.com/moc.php/328331)
X [Tristan Gordon - Space Shuttle](http://www.moc-pages.com/moc.php/280750)
X [" Luke " - Space Shuttle](http://www.moc-pages.com/moc.php/253460)
X [Dabin Kim - Space Shuttle Discovery](http://www.moc-pages.com/moc.php/109811)
X [Spencer R. - OV-103 Space Shuttle Discovery Overview](http://www.moc-pages.com/moc.php/4052)
X [Brian Hastings - SPACESHIPONE](http://www.moc-pages.com/moc.php/4408)
X [Brian Hastings - INTERNATIONAL SPACE STATION](http://www.moc-pages.com/moc.php/3062)
X [Brian Hastings - ISS - 05](http://www.moc-pages.com/moc.php/9170)
X [Brian Hastings - SPACE SHUTTLE LAUNCH PLATFORM](http://www.moc-pages.com/moc.php/14881)
X [Giovanni & Lennaert Seynhaeve - Earthrise](http://www.moc-pages.com/moc.php/286816)
X [Mark Stafford - Space Shuttle Discovery](http://www.moc-pages.com/moc.php/79805)
X [Leon Noppers - Space Shuttle](http://www.moc-pages.com/moc.php/249162)
X [Ben Watson - Large LEGO Space Shuttle](http://www.moc-pages.com/moc.php/137169)
X [Justin M - NASA Space Shuttle](http://www.moc-pages.com/moc.php/350988)
X [Bruce Lowell - Space Shuttle](http://www.moc-pages.com/moc.php/353347)
X [Alexander Exarhos - Old Space Shuttle](http://www.moc-pages.com/moc.php/3093)
X [MCLegoboy ! - Shuttle Launch 4x4 | 2017 MOCer Games](http://www.moc-pages.com/moc.php/439926)
X [daniel rojas - Launch Complex 39](http://www.moc-pages.com/moc.php/25026)
X [Nick White - NASA Space shuttle](http://www.moc-pages.com/moc.php/359407)
X [Donald Georgiades - Saturn V Launch Tower and Platform NASA LUT](http://www.moc-pages.com/moc.php/443614)
X [Donald Georgiades - Nasa Crawler](http://www.moc-pages.com/moc.php/444383)
X [craig lewis - Space Shuttle Discovery 8480](http://www.moc-pages.com/moc.php/52622)
X [Marco U - Space Shuttle Launch Pad and Crawler](http://www.moc-pages.com/moc.php/355221)
X [Andy LEGO Designer - Space shuttle](http://www.moc-pages.com/moc.php/67625)
X [peter markowski - Space Shuttle](http://www.moc-pages.com/moc.php/229246)
X [Intelligence Officer 5699 - Apolo LEM Lunar Exploration Module](http://www.moc-pages.com/moc.php/111979)
X [Marvin G. - Micro-scale NASA Space Shuttle](http://www.moc-pages.com/moc.php/79100)
X [Matt Mason - NASA Space shuttle](http://www.moc-pages.com/moc.php/42799)
X [arf 193 - space shuttle discovery](http://www.moc-pages.com/moc.php/39614)
X [Mr. Gorilla! (formally known as Luke W.) - The Space Shuttle](http://www.moc-pages.com/moc.php/333704)
X [Ethan Pooley - Space Shuttle @ Launch Pad 39a](http://www.moc-pages.com/moc.php/292675)
X [Matt Mason - Space Shuttle Atlantis](http://www.moc-pages.com/moc.php/238506)
X [Jason H - Space Shuttle Discovery](http://www.moc-pages.com/moc.php/207503)
X [Intelligence Officer 5699 - NASA, shuttle Discovery VERSION 1](http://www.moc-pages.com/moc.php/109481)
X [zachary lane - Space Shuttle with Boosters](http://www.moc-pages.com/moc.php/87727)
X [Nathaniel Ely - Space Shuttle with Satelite, Rocket Boosters and fuel tanks](http://www.moc-pages.com/moc.php/233161)
X [John Kim - Space Shuttle Enterprise](http://www.moc-pages.com/moc.php/8342)
X [Timo The Dutchman - Rocket with Space Shuttle](http://www.moc-pages.com/moc.php/271500)
X [Frankie T - my lego space shuttle](http://www.moc-pages.com/moc.php/252944)
X [Cooper Hull - Mini NASA Space Shuttle](http://www.moc-pages.com/moc.php/443062)
X [James Stewart - Orion](http://www.moc-pages.com/moc.php/158258)
X [kari jackson - International Space Station (70% complete)](http://www.moc-pages.com/moc.php/313282)
X [Ben Shuber - the NASA Space Shuttle](http://www.moc-pages.com/moc.php/171338)
X [Michele Barbiero - Space Shuttle Discovery 7470 - 2017 MOC](http://www.moc-pages.com/moc.php/438854)
X [tyler zanin - Space Shuttle](http://www.moc-pages.com/moc.php/278392)
X [Corey Campbell - Space shuttle](http://www.moc-pages.com/moc.php/395903)
X [Tammo Schröder - Micro Space Shuttle](http://www.moc-pages.com/moc.php/426281)
[Pedro Navarro - Nasa Space Shuttle (Saturn V Scale) - Lego Digital Designer Model](http://www.moc-pages.com/moc.php/447232) - This is recreation of KingsKnights Space Shuttle. There is link to ldd, however KingsKnights provided instructions, so this is outdated.
X [Tom PCfr0g - Lego mini Soyuz rocket](http://www.moc-pages.com/moc.php/443046)
X [Tom PCfr0g - Voyager 1](http://www.moc-pages.com/moc.php/443013)
X [Tom PCfr0g - Hubble Space Telescope](http://www.moc-pages.com/moc.php/443012)
X [Tom PCfr0g - Lego mini International Space Station](http://www.moc-pages.com/moc.php/442976)
X [Tom PCfr0g - Lego mini Apollo Saturn V](http://www.moc-pages.com/moc.php/442975)
X [Augie Krater - soyuz](http://www.moc-pages.com/moc.php/5766)
X [Aaron Boyer - Soyuz](http://www.moc-pages.com/moc.php/304086)
X [John Moffatt - Soyuz TMA-Q](http://www.moc-pages.com/moc.php/374958)

## Projects found on MECABRICKS

## Projects found on Reddit
X [Smazmats - Delta III](https://www.reddit.com/r/legoRockets/comments/dftq76/completed_delta_iii_with_accurate_staging/)
X [Smazmats - Mini LUT](https://www.reddit.com/r/legoRockets/comments/d4wa84/a_sneak_peek_at_the_first_draft_of_my_next/)
X [BedrockPanda - Bigger lunar landscape](https://www.reddit.com/r/legoRockets/comments/g3au5f/i_made_a_bigger_lunar_landscape_for_the_lem_from/)
X [Smazmats - Boieng's newly announced Human Lander System docked with an Orion capsule](https://www.reddit.com/r/legoRockets/comments/dstyjv/boiengs_newly_announced_human_lander_system/)
X [Smazmats - Dreamchaser](https://www.reddit.com/r/legoRockets/comments/crp7oe/1110_dreamchaser_space_plane_designed_for_moppes/)
X [uberdog01 - SpaceX Starship](https://www.reddit.com/r/legoRockets/comments/ftx28v/almost_done_with_my_1110_scale_starship_i_just/?utm_medium=android_app&utm_source=share)

## Other
X [Vostok 1 - With R-7 Rocket by Brickmania](https://www.brickmania.com/vostok-1-with-r-7-rocket/), originated on [Eurobricks](https://www.eurobricks.com/forum/index.php?/forums/topic/172745-mocmod-sputnik-2/)
X [Jussi Koskinen - Soyuz Fregat rocket](https://www.flickr.com/photos/j_koskinen/40110248543/in/photostream/)
X [LUT by teflon](https://www.bricksafe.com/pages/teflon/saturn-v-technic-launchpad)
X [LUT by JustusB](https://www.imperiumdersteine.de/index.php?threads%2Fsaturn-v-launch-umbilical-tower-startturm.47293%2F)
X [NASA Insight Lander](https://www.flickr.com/photos/cyndibourne/44247323130/)
X [Soyuz](https://www.ebay.ie/itm/262883327609) - not sold anymore.
X [Space Shuttle crawler transporter by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=453880)
X [Space shuttle launch complex OLD version by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=453882)
X [Space Shuttle Launch Complex 39 A By teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=508388)
X [Microscale Space Shuttle crawler transporter by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=517783)
X [Microscale Lanuch Complex 39 A by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=531983)
X [Nanoscale Space Shuttle Launch Complex by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=548100)
X [Space Shuttle Launch in microscale by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=552132)
X [Vehicle Assembly Building by teazza](http://www.brickshelf.com/cgi-bin/gallery.cgi?f=485285)
X [The Atlas ICBM, the pinnacle of fifties rocket science by Ralph at The Brothers Brick](https://www.brothers-brick.com/2019/06/01/the-atlas-icbm-the-pinnacle-of-fifties-rocket-science/)
X [LEGO Apollo Mission - Collector's custom set at Kickstarter](https://www.kickstarter.com/projects/442195851/lego-apollo-mission-collectors-custom-set/comments)
X [Zwolffium - X-37B](https://www.flickr.com/photos/187739424@N02/albums/72157713928118716)
X [Falcon 9 by Curt QuarquessoFollow](https://www.flickr.com/photos/curtquarquesso/50052343937/in/album-72157690268485570/)
X [LEGO R7 Rocket Family-02-01-Sputnik by Nick Porcino](https://www.pinterest.com/pin/496592296382097490/)
X [Brickule: Sputnik, Yutu Rover, Venera-9, Lunokhod Rover, Spirit/Opportunity, Pathfinder and Sojourner, Curiosity, Apollo rover](https://www.flickr.com/photos/25071763@N07/33040786685/in/photostream/)
