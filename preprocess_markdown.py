# preprocess markdown document
# reads from rockets.md, writes to stdout
# python is used because I am fed up with bash escaping in makefile, awk, egrep
# etc.

import sys
import re

# load markdown file:
file = open("rockets.md", "r")
mdfile = file.read()
file.close()

# Replace keys by anchor and image link using regular expression.
# Key is enclosed in !< and >!
# So this:
#    !<somerocket>!
# is changed to:
#    <a name="somerocket">![somerocket](img/somerocket.jpg)</a>
# and after pandoc markdown processing one get:
#    <td><a name="somerocket"><img src="img/somerocket.jpg" alt="somerocket" /></a></td>
mdfile = re.sub(r'!<(.*?)>!',
                r'<a href="https://kaerodot.gitlab.io/lego-rocket-mocs/index.html#\1" name="\1">![\1](img/\1.jpg "\1")</a>',
                mdfile)

# output to stdout:
sys.stdout.write(mdfile)
