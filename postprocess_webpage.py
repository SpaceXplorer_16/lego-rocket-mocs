# postprocess webpage
# reads from stdin, writes to public/index.html
# automatic addition of:
#   1, add class sortable for tables
#   2, current date
#   (removed: 3, git log history)
# python is used because I am fed up with bash escaping in makefile

import sys
import re
from datetime import date
from subprocess import check_output

#  #  get git log:
#  gitlog = check_output('git log --after=2019-09-23T23:30 --pretty=<tr><td>%ai</td><td>%s</td></tr>'.split()).decode(sys.stdout.encoding).strip()
#  #  gitlog = check_output('git log'.split()).decode(sys.stdout.encoding).strip()
#  #  gitlog = check_output('git log'.split())
#  #  get current day:
curdate = date.today().strftime('%Y.%m.%d')

# load temporary file:
#  file = open("tmp.html", "r")
tmpf = sys.stdin.read()
#  file.close()

# add class sortable to tables:
#  tmpf = tmpf.replace('<table ', '<table class="sortable" ')
tmpf = re.sub(r'<table.*>', '<table class="sortable">', tmpf)
# make replacements:
#  replace date placeholder by today:
tmpf = tmpf.replace('MAKEFILEDATEPLACEHOLDER', curdate, 1)
#  #  replace history placeholder by partial git log:
#  tmpf = tmpf.replace('MAKEFILEGITLOGPLACEHOLDER', gitlog, 1)

file = open("public/index.html", "w")
file.write(tmpf)
file.close
