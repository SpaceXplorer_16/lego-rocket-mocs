draft:
	# pandoc:
	#	-f: format is markdown github
	#	--css: css style sheet
	#	-s: standalone
	python3 preprocess_markdown.py | pandoc -f markdown_github+markdown_in_html_blocks --css pandoc.css -s | python3 postprocess_webpage.py
	pandoc -f markdown_github+markdown_in_html_blocks --css pandoc.css -s -o public/candidates.html candidates.md
